package com.rbyakov.clevertec.api;

import com.google.gson.JsonObject;
import com.rbyakov.clevertec.api.models.ClevertecForm;
import com.rbyakov.clevertec.api.models.MetaData;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ClevertecApi {

    @POST("/tt/meta")
    Observable<ClevertecForm> getMetaInfo();

    @POST("tt/data")
    Observable<JsonObject> sendData(@Body MetaData form);
}
