package com.rbyakov.clevertec.api.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Home on 27.07.2017.
 */

public class ClevertecForm {

    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @SerializedName("fields")
    private List<MetaInfo> fields;

    public List<MetaInfo> getFields() {
        return fields;
    }

    public void setFields(List<MetaInfo> fields) {
        this.fields = fields;
    }
}
