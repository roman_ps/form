package com.rbyakov.clevertec.api.models;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;

/**
 * Created by Home on 27.07.2017.
 */

public class MetaInfo {

    @SerializedName("title")
    private String title;

    @SerializedName("name")
    private String name;

    @SerializedName("type")
    private MetaTypes type;

    private String currentValue;

    @SerializedName("values")
    private HashMap<String, String> values;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MetaTypes getType() {
        return type;
    }

    public void setType(MetaTypes type) {
        this.type = type;
    }

    public HashMap<String, String> getValues() {
        return values;
    }

    public void setValues(HashMap<String, String> values) {
        this.values = values;
    }

    public String getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue(String currentValue) {
        this.currentValue = currentValue;
    }
}
