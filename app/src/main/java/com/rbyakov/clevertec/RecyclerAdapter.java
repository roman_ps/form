package com.rbyakov.clevertec;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.rbyakov.clevertec.api.models.MetaInfo;

import java.util.LinkedList;
import java.util.List;

import static android.R.attr.data;

/**
 * Created by Home on 30.07.2017.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    public static final int ITEM_TYPE_TEXT = 0;
    public static final int ITEM_TYPE_NUMERIC = 1;
    public static final int ITEM_TYPE_LIST = 2;

    public List<MetaInfo> fields;
    private Context mContext;

    public RecyclerAdapter(List<MetaInfo> fields) {
        this.fields = fields;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View v = null;
        switch (viewType) {
            case ITEM_TYPE_TEXT:
                v = inflater.inflate(R.layout.text_item, parent, false);
                break;
            case ITEM_TYPE_NUMERIC:
                v = inflater.inflate(R.layout.numeric_item, parent, false);
                break;
            case ITEM_TYPE_LIST:
                v = inflater.inflate(R.layout.list_item, parent, false);
                break;
        }
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mTextView.setText(fields.get(position).getTitle());
        // if views more difficult use different viewholders
        if (getItemViewType(position) == ITEM_TYPE_LIST) {

            String[] data = fields.get(position).getValues().values().toArray(new String[0]);

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, data);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            holder.mSpinner.setAdapter(adapter);
            // некрасиво
            if (fields.get(position).getCurrentValue() != null) {
                for (int i = 0; i < data.length; i++) {
                    if (fields.get(position).getCurrentValue().equals(data[i])) {
                        holder.mSpinner.setSelection(i);
                        break;
                    }
                }
            } else {
                holder.mSpinner.setSelection(0);
            }

            holder.mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    fields.get(position).setCurrentValue(data[i]);
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        } else {
            if (fields.get(position).getCurrentValue() != null) {
                holder.mEditText.setText(fields.get(position).getCurrentValue());
            }
            // if many values in recycler, recycler return only visible elements values
            holder.mEditText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    fields.get(position).setCurrentValue(charSequence.toString());
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        switch (fields.get(position).getType()) {
            case LIST:
                return ITEM_TYPE_LIST;
            case NUMERIC:
                return ITEM_TYPE_NUMERIC;
            default:
                return ITEM_TYPE_TEXT;
        }
    }

    @Override
    public int getItemCount() {
        return fields.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView mTextView;
        public EditText mEditText;
        public Spinner mSpinner;

        public ViewHolder(View v) {
            super(v);
            mTextView = (TextView) v.findViewById(R.id.label);
            mEditText = (EditText) v.findViewById(R.id.edit_text);
            mSpinner = (Spinner) v.findViewById(R.id.spinner);
        }
    }
}
