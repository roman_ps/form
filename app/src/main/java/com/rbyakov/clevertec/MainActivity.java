package com.rbyakov.clevertec;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.rbyakov.clevertec.api.models.ClevertecForm;
import com.rbyakov.clevertec.api.models.MetaData;
import com.rbyakov.clevertec.api.models.MetaInfo;
import com.trello.rxlifecycle2.components.support.RxAppCompatActivity;

import java.util.HashMap;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends RxAppCompatActivity {

    private static final String LIST_STATE_KEY = "ListState";
    private static final String VALUES = "Values";
    private String LOADING_KEY = "isLoading";

    private ProgressDialog myDialog;
    private Observable<ClevertecForm> getFormObservable;
    private Observable<JsonObject> sendFormObservable;
    private Disposable subscription;

    private RecyclerView mRecyclerView;
    private RecyclerAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private Parcelable mListState;
    private boolean isLoading = false;
    private List<MetaInfo> fields;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerView = (RecyclerView) findViewById(R.id.content);

        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        if (savedInstanceState == null) {
            getFormObservable = ClevertecApp.instance.api
                    .getMetaInfo()
                    .compose(bindToLifecycle()) // control recreate
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread());

            subscription = getFormObservable.subscribe(form -> {
                setTitle(form.getTitle());
                hideDialog();
                fields = form.getFields();
                mAdapter = new RecyclerAdapter(fields);
                mRecyclerView.setAdapter(mAdapter);
            }, error -> {
                hideDialog();
                error.printStackTrace();
                Snackbar.make(findViewById(android.R.id.content), R.string.error, Snackbar.LENGTH_SHORT).show();
            });

            showDialog();
        } else {
            fields = new Gson().fromJson(savedInstanceState.getString(VALUES), new TypeToken<List<MetaInfo>>() {
            }.getType());
            mAdapter = new RecyclerAdapter(fields);
            mRecyclerView.setAdapter(mAdapter);
        }
    }

    protected void onRestoreInstanceState(Bundle state) {
        super.onRestoreInstanceState(state);

        // Retrieve list state and list/item positions
        if (state != null) {
            isLoading = state.getBoolean(LOADING_KEY);
            if (isLoading) {
                showDialog();
            }

            mListState = state.getParcelable(LIST_STATE_KEY);
        }


    }

    protected void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);

        // save isLoading
        state.putBoolean(LOADING_KEY, isLoading);

        // Save list state
        mListState = mLayoutManager.onSaveInstanceState();
        state.putParcelable(LIST_STATE_KEY, mListState);
        if (mAdapter.fields != null)
            state.putString(VALUES, new Gson().toJson(mAdapter.fields));
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mListState != null) {
            mLayoutManager.onRestoreInstanceState(mListState);
        }
    }

    public void showDialog() {
        isLoading = true;
        myDialog = new ProgressDialog(MainActivity.this);
        myDialog.setMessage("Loading ...");
        myDialog.setCancelable(false);
        myDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                subscription.dispose();
                dialog.dismiss();
            }
        });
        myDialog.show();
    }

    public void hideDialog() {
        isLoading = false;
        if (myDialog != null) {
            myDialog.dismiss();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.custommenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        showDialog();

        HashMap<String, String> formData = new HashMap<>();

        if (item.getItemId() == R.id.upload) {
            for (MetaInfo info : mAdapter.fields
                    ) {
                formData.put(info.getName(), info.getCurrentValue());
                System.out.println(info.getCurrentValue());
            }

            sendFormObservable = ClevertecApp.instance.api
//                    .sendData("{\"form\":{\"text\": \"sdafasdf\",\"numeric\": \"12\",\"list\": \"Первое значение\"}}")
                    .sendData(new MetaData(formData))
                    .compose(bindToLifecycle())
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread());

            subscription = sendFormObservable.subscribe(response -> {
                hideDialog();

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(response.get("result").toString())
                        .setTitle(getString(R.string.response));
                AlertDialog dialog = builder.create();

                dialog.show();
            }, error -> {

                hideDialog();

                error.printStackTrace();
                Snackbar.make(findViewById(android.R.id.content), error.getMessage(), Snackbar.LENGTH_SHORT).show();
            });
        }
        return super.onOptionsItemSelected(item);
    }
}
